﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace Aplicacion_Clinica.Models
{
    public class Persona
    {
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Identificación:")]
        public int id { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Nombre:")]
        public string nombre { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Apellido:")]
        public string apellido { get; set; }
    }
}